package simulations

import scala.concurrent.duration._

import io.gatling.core.Predef._ // 2
import io.gatling.http.Predef._

class SITPEGAHome extends Simulation { // 3

  val httpProtocol = http // 4
    .baseUrl("https://pega-web-sit.apps.openshift-test1-dca1.csda.gov.au/prweb/app/default/2mCjkZJmJzIb2YFZHOYfCw%28%28*/!STANDARD") // 5
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") // 6
    .doNotTrackHeader("1")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0")

  val scn = scenario("SITPEGAHome") // 7
    .exec(http("request_1") // 8
      .get("/")) // 9
    .pause(5) // 10

  setUp( // 11
    scn.inject(atOnceUsers(1)) // 12
  ).protocols(httpProtocol) // 13
}

